import { ADD_TAG, EDIT_TAG } from '../actions';
import { TAGS } from '../data';

const INITIAL_STATE = {
    tags: TAGS,
  }
 
export default function (state = INITIAL_STATE, action) {
    
    if(action.type === ADD_TAG) {
        const { newTag } = action;
        const { tags } = state;

        tags.push({...newTag, id: tags.length +1 });
        return {
            ...state,
            tags
        };
    }

    if(action.type === EDIT_TAG) {
        const { editTag } = action;
        
        const tags = state.tags.map(tag=>{

                if(tag.id === editTag.id){
                    return {
                        ...tag,
                        name: editTag.name
                        
                    }
                }
            return  tag;
        })

        return {
                ...state,
                    tags
                };
    }

   return state;
};

