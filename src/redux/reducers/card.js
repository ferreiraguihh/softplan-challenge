
import { CARDS } from '../data';
import { REMOVE_TAG_CARD, ADD_TAG_CARD, FILTER_CARD_TAG} from '../actions';

const initialState = {
    initialCards: CARDS,
    cards: CARDS,
};

const reducer = (state = initialState, action) => {

    if (action.type === REMOVE_TAG_CARD) {

        const {idCard, idTag } = action;
    
        const cards = state.cards.map(card =>{
            if(idCard !== card.id){
                return card;
            }

            return  {
                ...card,
                tags: card.tags.filter(tag => tag.id !== idTag)
            }
        })

        return {...state,
                cards
        } 
    }

    if (action.type === ADD_TAG_CARD) {
        const {idCard, idTag } = action;
    
        const cards = state.cards.map(card =>{
            
            if(idCard === card.id){
                
                card.tags.push({ id: idTag });
                return card;
            }
        
            return card;
                
        });

        return {
            ...state,
            cards
        } 
    }


    if(action.type === FILTER_CARD_TAG){
        const {idTag} = action;

        if(!idTag){
            return {
                ...state,
                cards: CARDS,
            };
        }

        const cards = CARDS.filter((card) =>  card.tags.find(tag=> tag.id === idTag));
        return {
            ...state,
            cards
        }
    }

    return state;
};


export default reducer;


































