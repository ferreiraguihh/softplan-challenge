export const ADD_TAG = 'ADD_TAG';
export const ADD_TAG_CARD = 'ADD_TAG_CARD';
export const REMOVE_TAG_CARD = 'REMOVE_TAG_CARD';
export const FILTER_CARD_TAG = 'FILTER_CARD_TAG';
export const SEARCH_CARD = 'SEARCH_CARD';
export const EDIT_TAG = 'EDIT_TAG';
