import {combineReducers} from 'redux';
import Tag from './reducers/tag';
import Card from './reducers/card';


export default combineReducers({
    Tag,
    Card
});
