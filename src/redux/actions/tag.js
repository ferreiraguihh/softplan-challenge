import { ADD_TAG, EDIT_TAG } from '../actions';

export function addTag(newTag){
    return {
      type: ADD_TAG,
      newTag,
    };
  }


export function editTag(editTag){
  
    return {
      type: EDIT_TAG,
      editTag,
    };
  }

