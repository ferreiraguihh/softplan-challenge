import { REMOVE_TAG_CARD, ADD_TAG_CARD, FILTER_CARD_TAG} from '../actions';
export const removeTag = (idCard, idTag) => {
    return {
        type: REMOVE_TAG_CARD,
        idTag,
        idCard
    }

};

export const addTag = (idCard, idTag) => {
    return {
        type: ADD_TAG_CARD,
        idTag,
        idCard
    }
};


export const filterCardByTag = (idTag) =>{
        return  {
            type: FILTER_CARD_TAG,
            idTag,
        }
}

