import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import * as actions from "../../redux/actions/card";
import styles from "./styles";
import "./card.css";

function filterByValue(array, value) {
  return array.filter(
    data =>
      JSON.stringify(data)
        .toLowerCase()
        .indexOf(value.toLowerCase()) !== -1
  );
}

class Cards extends React.Component {
  state = {
    openForm: false,
    cards: {},
    openTickt: null
  };

  handleOpenForm = () => {
    const { openForm } = this.state;
    this.setState({ openForm: !openForm });
  };

  handleClick = openTickt => {
    this.setState({ openTickt}, () =>{
      document.getElementById('modal').focus()
    });
  };

  getTagMarked = card => {
    const { tags, removeTag } = this.props;
    return card.tags.map(currentTag => {
      const tag = tags.find(tag => tag.id === currentTag.id);
      return (
        <div
          className="name-tag-cads"
          style={{ backgroundColor: tag.background }}
        >
          <div>
            <p>{tag.name}</p>
          </div>
          <div
            onClick={() => {
              removeTag(card.id, tag.id);
            }}
          >
            <i class="material-icons close">
              close
          </i>
          </div>
        </div>
      );
    });
  };

  render() {
    const { classes, cards, tags, addTag, search } = this.props;
    const { openTickt } = this.state;
    return (
      <Fragment>
        <main
          className={classes.content}
        >
          <div className={classes.toolbar} />

          <div className="moviment">
            {filterByValue(cards, search).map(card => (
              <div className="card">
                <div className="info-all-card">
                  <div className="partes-card">
                    <i class="material-icons positive">add_circle</i>
                    {card.partes.ativa.name}

                    <i class="material-icons negative">remove_circle</i>
                    {card.partes.passiva.name}

                  </div>

                  <div className="assunto-card">
                    <p>{card.classe}</p>
                    <p>-</p>
                    <p>
                      <b>{card.assunto}</b>
                    </p>
                  </div>

                  <div className="numero-card">
                    <p>{card.numero}</p>
                  </div>
                </div>
                <div className="pasta-card">
                  <i class="material-icons folder">folder</i>

                  <p>Abrir pasta</p>
                </div>
 
                <div className="tags-card">
                  <div
                    onClick={() => {
                      this.handleClick(card.id);
                    }}
                  >
                    <i class="material-icons addtag">label</i>
                  </div>
                  {/* onClick={() => this.setState({ openTickt: null })} */}

                  {openTickt === card.id && (
                    <div
                      className="modal-ticket"
                      onBlur={() => this.setState({ openTickt: 0 })}
                      tabIndex={0}
                      id="modal"
                    >
                      <p className="modal-title">Etiquetar como:</p>
                      {tags.map(tag => (
                        <div
                          onClick={() => addTag(card.id, tag.id)}
                          onChange={() => this.setState({ openTickt: null })}
                          className="modal-name"
                          style={{
                            backgroundColor: tag.background,
                            opacity: `${
                              card.tags.find(t => t.id === tag.id) ? "0.3" : "1"
                              }`
                          }}
                        >
                          <p>{tag.name}</p>
                        </div>
                      ))}
                    </div>
                  )}
                  {this.getTagMarked(card)}
                </div>
              </div>
            ))}
          </div>
        </main>
      </Fragment>
    );
  }
}

Cards.propTypes = {
  classes: PropTypes.object.isRequired,
  cards: PropTypes.arrayOf(PropTypes.object),
  removeTag: PropTypes.func.isRequired,
  addTag: PropTypes.func.isRequired,
  search: PropTypes.string
};

Cards.defaultProps = {
  search: ""
};

export default connect(
  ({ Card, Tag }) => ({
    cards: Card.cards,
    tags: Tag.tags
  }),
  dispatch => bindActionCreators(actions, dispatch)
)(withStyles(styles)(withRouter(Cards)));
