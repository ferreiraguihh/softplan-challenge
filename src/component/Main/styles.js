const drawerWidth = 240;

export default theme => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
    backgroundColor: '#F4F4F4'
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: '#0063B1!important',
  },
  tool:{
    display: 'flex',
    alignItems: 'space-between',
    justifyContent: 'space-between',
    fontFamily: 'Semibold',
    fontSize: 20,
  },
  appBar2: {
    marginLeft: drawerWidth,
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
    },
    top: '63px'
  },
    menuButton: {
      marginRight: 20,
      [theme.breakpoints.up('sm')]: {
        display: 'none',
      },
    },
  
  
  });