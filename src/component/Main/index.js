import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Hidden from "@material-ui/core/Hidden";
import Toolbar from "@material-ui/core/Toolbar";
import Card from "../Card";
import * as actionsTag from "../../redux/actions/tag";
import { filterCardByTag } from "../../redux/actions/card";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { bindActionCreators } from "redux";
import styles from "./styles";
import "./main.css";
import Buscar from "../../icons/Search.svg";
import processos from "../../icons/ArchiveFiles.svg";
import { GithubPicker } from "react-color";

const actions = {
  ...actionsTag,
  filterCardByTag
};

const initialNewTag = {
  background: "#000000"
};

function mapCountTag(tags, cards) {
  const novoArrayComTotal = tags.map(tag => {
    let total = 0;
    cards.forEach(card => {
      const founded = card.tags.find(element => element.id === tag.id);
      if (founded != null) {
        total++;
      }
    });

    return {
      ...tag,
      total
    };
  });

  return novoArrayComTotal;
}

class Main extends React.Component {
  state = {
    mobileOpen: false,
    open: false,
    currentTag: {},
    isShowPicker: false,
    newTag: initialNewTag,
    search: "",
    isNewTag: null,
    isSelect: true,
  };

  handleDrawerToggle = () => {
    this.setState(state => ({ mobileOpen: !state.mobileOpen }));
  };

  handleClick = tag => {
    this.setState({ currentTag: { ...tag } });
  };

  changeNameNewtag = name => {
    const { newTag } = this.state;
    this.setState({ newTag: { ...newTag, name } });
  };

  addTag = () => {
    const { newTag, isNewTag } = this.state;

    this.setState({ newTag: initialNewTag, isNewTag: !isNewTag });
    this.props.addTag(newTag);

  };

  handleChangeComplete = color => {
    const { newTag } = this.state;
    this.setState({
      newTag: {
        ...newTag,
        background: color.hex
      },
      isShowPicker: false
    });
  };

  changeEditName = name => {
    const { currentTag } = this.state;

    this.setState({
      currentTag: {
        ...currentTag,
        name
      }
    });
  };

  editTag = () => {
    const { currentTag } = this.state;
    this.setState({ currentTag: {} });
    this.props.editTag(currentTag);
  };

  render() {
    const { classes, tags, filterCardByTag, totalCards } = this.props;
    const { currentTag, newTag, isShowPicker, search, isNewTag, isSelect } = this.state;

    return (
      <div className="root">
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar className={classes.tool}>
            <div>
              <p>APP</p>
            </div>

            <div>
              <i class="material-icons icon-login">
                account_circle
              </i>
            </div>
          </Toolbar>
        </AppBar>

        <AppBar position="fixed" className={classes.appBar2} >
          <Toolbar className="toolbar">
            <img src={Buscar} alt="buscar" className="img-busca" />
            <input
              className="input-buscar"
              placeholder="Buscar"
              value={search}
              onChange={({ target }) => {
                this.setState({ search: target.value });
              }}
            />
          </Toolbar>
        </AppBar>
        <CssBaseline />

        <nav className={classes.drawer}>
          <Hidden xsDown implementation="css">
            <Drawer
              className={classes.drawer}
              variant="permanent"
              classes={{
                paper: classes.drawerPaper,
              }}
            >
              <div>
                <div className={classes.toolbar} style={{ marginTop: 62 }} />
                <div className="menuAjust">
                  <p className="p-process">
                    <b>Processos</b>
                  </p>
                </div>

                <div
                  className="todo-process"
                  onClick={() => {
                    filterCardByTag(null);
                  }}

                  style={{
                    borderLeft: `${
                      isSelect === true && "4px solid #0078D7"
                      }`,
                    backgroundColor: `${
                      isSelect === true && "#EAEAEA"
                      }`
                  }}
                >
                  <img src={processos} alt="Processos" className="ticket-icon" />
                  <p className="p-todos-process">
                    <b>Todos Processos</b>
                  </p>
                  <p className="value-process">{totalCards}</p>
                </div>

                <div className="menuAjust">
                  <p className="p-process">
                    <b>Etiquetas</b>
                  </p>
                </div>
                <List>
                  {tags.map(tag => (
                    <div>
                      {currentTag.id !== tag.id && (
                        <div className="list-name">
                          <div
                            className="icon-color"
                            style={{ backgroundColor: tag.background }}
                            onClick={() => {
                              filterCardByTag(tag.id);
                            }}
                          />

                          <div
                            key={tag.id}
                            onClick={() => {
                              this.handleClick(tag);
                            }}
                            className="tags-name"
                          >
                            {tag.name}
                          </div>

                          <p
                            className="value-ticket"
                            onClick={() => {
                              filterCardByTag(tag.id);
                            }}
                          >
                            {tag.total}
                          </p>
                        </div>
                      )}
                      {currentTag.id === tag.id && (
                        <div className="display-flex">
                          <input
                            type="text"
                            className="inputTicket"
                            value={currentTag.name}
                            onChange={({ target }) => this.changeEditName(target.value)}
                            autoFocus
                            onBlur={this.editTag}
                          />
                          <i
                            className="material-icons icon-done"
                            style={{ cursor: "pointer" }}
                          >
                            done
                  </i>
                        </div>
                      )}
                    </div>
                  ))}

                  {isNewTag === true && (
                    <div className="new-tag">
                      <div
                        className="icon-color"
                        style={{ backgroundColor: newTag.background }}
                        onClick={() => {
                          this.setState({ isShowPicker: !isShowPicker });
                        }}
                      />
                      <input
                        type="text"
                        className="inputTicket-2"
                        placeholder="Digite aqui"
                        value={newTag.name}
                        onChange={({ target }) => {
                          this.changeNameNewtag(target.value);
                        }}
                      />

                      {isShowPicker && (
                        <GithubPicker
                          className="github-pick"
                          color={newTag.background}
                          onChangeComplete={this.handleChangeComplete}
                        />
                      )}

                      <div>
                        <i class="material-icons add-tag-icon" onClick={this.addTag}>
                          add_circle
                </i>
                      </div>
                    </div>
                  )}
                  <div
                    className="div-criar-ticket"
                    onClick={() => this.setState({ isNewTag: !isNewTag })}
                  >
                    <i class="material-icons add-icon">label</i>
                    <p className="criar-ticket">Criar etiqueta </p>
                  </div>
                </List>
              </div>
              </Drawer>
          </Hidden>
        </nav>
        <Card search={search} />
      </div>
    );
  }
}

Main.propTypes = {
  classes: PropTypes.object.isRequired,
  tags: PropTypes.arrayOf(PropTypes.object).isRequired,
  addTag: PropTypes.func.isRequired,
  editTag: PropTypes.func.isRequired,
  filterCardByTag: PropTypes.func.isRequired,
  totalCards: PropTypes.number.isRequired
};

export default connect(
  ({ Tag, Card }) => ({
    tags: mapCountTag(Tag.tags, Card.initialCards),
    totalCards: Card.initialCards.length
  }),
  dispatch => bindActionCreators(actions, dispatch)
)(withStyles(styles, { withTheme: true })(withRouter(Main)));
