import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {applyMiddleware, compose, createStore} from 'redux';
import {BrowserRouter as Router} from 'react-router-dom';
import thunk from 'redux-thunk';
import createBrowserHistory from 'history/createBrowserHistory'
import rootReducer from './redux';
import Grid from "@material-ui/core/Grid/Grid";
import Main from "./component/Main"
import './index.css'
import * as serviceWorker from './serviceWorker';

const customHistory = createBrowserHistory();

const store = createStore(
    rootReducer,
    {},
    compose(
        applyMiddleware(thunk),
        window.devToolsExtension ? window.devToolsExtension() : f => f
    )
);

ReactDOM.render(
    <Provider store={store} >
        <Router history={customHistory}>
            <Grid>
                <Main style={{backgroundColor: 'black'}}/>
            </Grid>
        </Router>

    </Provider>,
    document.getElementById('root'));


serviceWorker.unregister();
